#!/usr/bin/env bash

# Prelude.
SCRIPT_DIRECTORY="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
PROJECT_DIRECTORY="$(git -C "$SCRIPT_DIRECTORY" rev-parse --show-toplevel)"

function inform {
  echo "$@" "$@"
}

# Arguments to this script.
TOOLCHAIN_ARCHIVE_URL="${TOOLCHAIN_ARCHIVE_URL:-"https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-rm/6_1-2017q1/gcc-arm-none-eabi-6-2017-q1-update-linux.tar.bz2"}"
TOOLCHAINS_DIRECTORY="${TOOLCHAINS_DIRECTORY:-"$PROJECT_DIRECTORY/toolchains"}"

# Some derived variables.
TOOLCHAIN_ARCHIVE_NAME="$(basename "$TOOLCHAIN_ARCHIVE_URL")"
TOOLCHAIN_ARCHIVE_FILE="$TOOLCHAINS_DIRECTORY/$TOOLCHAIN_ARCHIVE_NAME"

if [[ ! -d "$TOOLCHAINS_DIRECTORY" ]]; then
  inform mkdir --parents "$TOOLCHAINS_DIRECTORY"
fi

# Download the archive if it doesn't already exist.
if [[ ! -f "$TOOLCHAIN_ARCHIVE_FILE" ]]; then
  inform wget --verbose --output-document "$TOOLCHAIN_ARCHIVE_FILE" "$TOOLCHAIN_ARCHIVE_URL"
fi

# Extract the archive, skipping files that already exist.
inform tar --extract --skip-old-files --directory "$TOOLCHAINS_DIRECTORY" --file "$TOOLCHAIN_ARCHIVE_FILE"

