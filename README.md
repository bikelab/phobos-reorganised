# Goal

The phobos repository has too many locations where source files live. There are
also some CMake cache issues due to External Project use that cause some things
not to be built when they should. This repository is an attempt at a simpler
organisation.

## Todo 

 * Add and use external dependency
 * Add tests for desktop
 * Add embedded example
 * Add tests for embedded
